tinymce.IconManager.add('ruby-annotation', {
  icons: {
    'ruby': '<svg version="1.0" width="21.3" height="21.3" viewBox="0 0 16 16"><path d="M1.5 4C-.4 6-.3 6.2 3.8 10.3L8 14.5l4.2-4.2C16.3 6.2 16.4 6 14.5 4c-2.6-2.8-10.4-2.8-13 0zm8.3-.3C8.5 4 8 5.2 8 7.8v3.7L5.3 8.8C3.1 6.5 2.9 5.9 4 4.6c.7-.9 2.6-1.5 4.4-1.5 1.7.1 2.3.3 1.4.6z"/></svg>',
  }
});